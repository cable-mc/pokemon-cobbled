### Additions

- Added the ability to ride Pokémon by shift-right clicking and selecting the ride option.